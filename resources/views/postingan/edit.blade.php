@extends('layouts.master')


@section('title')
    <h1>Edit Postingan Anda!</h1>
@endsection

@section('content')
    <div>
        <form action="/postingan/{{$postingan->id}}" enctype="multipart/form-data" method="POST">
            @csrf
            @method('put')
            <input type="hidden" name="users_id" value="">
            <div class="form-group">
                <label>Tulisan Anda</label>
                    <textarea name="tulisan" class="from-control" plasecholder="Masukan Tulisan!" cols="227,5" rows="10">{{$postingan->tulisan}}</textarea>
                        @error('tulisan')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
            </div>

            <div class="form-group">
                <label>Quote Anda</label><br>
                    <textarea name="quote" class="from-control" plasecholder="Masukan Quote!" cols="100" rows="5"> {{$postingan->quote}} </textarea>
                        @error('quote')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
            </div>

            <div class="form-group">
                <label>Gambar Anda</label>
                    <input type="file" class="form-control-file" name="gambar">
            </div>
                @error('gambar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

            <button type="submit" class="btn btn-primary">Edit Postingan</button>
        </form>     
    </div>
@endsection