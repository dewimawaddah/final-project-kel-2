@extends('layouts.master')


@section('title')
    Postingan Anda
@endsection

@section('content')

<a href="/postingan/create" class="btn btn-primary mb-5">Tambah Postingan</a>

<div class="row">
    @foreach ($postingan as $value)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{asset('uploads/postingan/'. $value->gambar,)}}" alt="Card image cap">
                <div class="card-body">
                <p class="card-title mb-3">{{ Str::limit($value->tulisan, 50) }}</p>
                <p class="card-text">{{ Str::limit($value->quote, 100) }}</p>
                <a href="/postingan/{{$value->id}}" class="btn btn-primary mb-2">Lihat Postingan</a>
                <br>
                <a href="/postingan/{{$value->id}}/edit" class="btn btn-info mb-2">Edit Postingan</a>
                <br>
                <form action="/postingan/{{$value->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger">
                </form>

                </div>
            </div>    
        </div>
    @endforeach
</div>
    
@endsection