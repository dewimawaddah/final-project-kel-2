

@extends('layouts.master')


@section('title')
<h1>Postingan Anda</h1>
@endsection

@section('content')

    <img class="card-img-top" src="{{asset('uploads/postingan/'. $postingan->gambar)}}" style="width:400px" alt="Card image cap">
    <h2>{{$postingan->tulisan}}</h2>
    <br>
    <h4>{{$postingan->quote}}</h4>
    
    <br>

    <div>
        <form action="/postingan" enctype="multipart/form-data" method="POST">
            @csrf
            
            <div class="form-group">
                <label>Komentar anda</label>
                    <textarea name="tulisan" class="from-control" plasecholder="Masukan Komentar Anda" cols="227,5" rows="10"></textarea>
                        @error('tulisan')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
            </div>

            <button type="submit" class="btn btn-primary">Komen</button>
        </form>     
    </div>

@endsection
