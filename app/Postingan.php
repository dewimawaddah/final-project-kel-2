<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    protected $table = 'postingan';

    protected $fillable = ['tulisan', 'gambar', 'quote','users_id' ];
    
    public $timestamps = false;
}
