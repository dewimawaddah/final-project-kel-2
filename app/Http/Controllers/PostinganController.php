<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Postingan;
use File;

class PostinganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postingan = Postingan::all();
        return view('postingan.index', compact('postingan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('postingan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tulisan' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png|max:2200',
            'quote' => 'required',
            'users_id' => 'required',
        ]);

        $poster = $request->gambar;
        $new_poster = time().'-'.$poster->getClientOriginalName();

        $postingan = Postingan::create([
            'tulisan'=>$request->tulisan,
            'gambar'=>$new_poster,
            'quote'=>$request->quote,
            'users_id'=>$request->users_id,
        ]);

        $poster->move('uploads/postingan/', $new_poster);
        return redirect('/postingan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postingan = Postingan::findorfail($id);
        return view('postingan/show', compact('postingan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postingan = Postingan::findorfail($id);
        return view('postingan/edit', compact('postingan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tulisan' => 'required',
            'gambar' => 'mimes:jpeg,jpg,png|max:2200',
            'quote' => 'required',
            'users_id' => 'required',
        ]);

        $postingan = Postingan::findorfail($id);
        
        if ($request->has('gambar')) {
            $path = "uploads/postingan";
            File::detele($path . $postingan->gambar);
            $poster = $request->gambar;
            $new_poster = time() . ' - '. $poster->getClientOriginalName();
            $poster->move('uploads/postingan/', $new_poster);
            $postingan_data = [
                'tulisan'=>$request->tulisan,
                'gambar'=>$new_poster,
                'quote'=>$request->quote,
                'users_id'=>$request->users_id,
            ];
        } else {
            $postingan_data = [
                'tulisan'=>$request->tulisan,
                'quote'=>$request->quote,
                'users_id'=>$request->users_id,
            ];
        }
        $postingan->update($postingan_data);
        return redirect()->route('postingan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $postingan = Postingan::findorfail($id);
        $postingan->delete();
        
        $path = "uploads/postingan/";
        File::delete($path . $postingan->gambar);

        return redirect()->route('postingan.index');
    }
}
